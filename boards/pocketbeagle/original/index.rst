.. _pocketbeagle-home:

PocketBeagle
###################

.. admonition:: OSHWA Certification mark

    .. figure:: images/OSHW_mark_US000083.*
        :width: 200
        :target: https://certification.oshwa.org/us000083.html
        :alt: PocketBeagle OSHW mark

.. note::

   This work is licensed under a `Creative Commons Attribution-ShareAlike
   4.0 International License <http://creativecommons.org/licenses/by-sa/4.0/>`__

   Hardware design files can be found at https://git.beagleboard.org/beagleboard/pocketbeagle

.. tip::
    Make sure to read and accept all the terms & condition provided in the :ref:`boards-terms-and-conditions` page. 
    
    Use of either the boards or the design materials constitutes agreement to the T&C including any 
    modifications done to the hardware or software solutions provided by beagleboard.org foundation.

PocketBeagle is an ultra-tiny-yet-complete open-source USB-key-fob computer. 
PocketBeagle features an incredible low cost, slick design and simple usage, 
making PocketBeagle the ideal development board for beginners and professionals alike. 


.. image:: images/PocketBeagle-size-compare-small.*
   :width: 598                                     
   :align: center                                   
   :alt: PocketBeagle                                 


.. toctree::
   :maxdepth: 1

   ch01
   ch02
   ch03
   ch04
   ch05
   ch06
   ch07
   ch08
   ch09
   ch10
   ch11
