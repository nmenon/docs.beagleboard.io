.. _beaglev-fire-support:

Support
#######

.. _beaglev-fire-certifications:

Certifications and export control
*********************************

Export designations
===================

.. todo:: update details

* HS: 
* US HS: 
* EU HS: 

Size and weight
===============

.. todo:: update details

* Bare board dimensions: 
* Bare board weight: 
* Full package dimensions: 
* Full package weight: 

.. _beaglev-fire-support-documentation:

Additional documentation
************************

Hardware docs
==============

For any hardware document like schematic diagram PDF, 
EDA files, issue tracker, and more you can checkout the 
`BeagleV-Fire design repository <https://git.beagleboard.org/beaglev-fire/beaglev-fire/>`_.

Software docs
==============

For BeagleV-Fire specific software projects you can checkout all the 
`BeagleV-Fire project repositories group <https://git.beagleboard.org/beaglev-fire>`_.

Support forum
=============

For any additional support you can submit your queries on our forum,
https://forum.beagleboard.org/c/beaglev

Pictures
========

.. _beaglev-fire-change-history:

Change History
***************

.. note:: 
    This section describes the change history of this document and board. 
    Document changes are not always a result of a board change. A board 
    change will always result in a document change.

.. _beaglev-fire-document-changes:

Document Changes
==================

For all changes, see https://git.beagleboard.org/docs/docs.beagleboard.io. Frozen releases tested against
specific hardware and software revisions are noted below.

.. table:: BeagleV-Fire document change history

    +---------+------------------------------------------------------------+----------------------+-------+
    | Rev     |   Changes                                                  | Date                 |    By |
    +=========+============================================================+======================+=======+
    |         |                                                            |                      |       |
    +---------+------------------------------------------------------------+----------------------+-------+

.. _beaglev-fire-board-changes:

Board Changes
==============

For all changes, see https://git.beagleboard.org/beaglev-fire/beaglev-fire/. Versions released into production
are noted below.

.. table:: BeagleV-Fire board change history

    +---------+------------------------------------------------------------+----------------------+-------+
    | Rev     |   Changes                                                  | Date                 |    By |
    +=========+============================================================+======================+=======+
    |         |                                                            | 2023-03-08           |       |
    +---------+------------------------------------------------------------+----------------------+-------+

